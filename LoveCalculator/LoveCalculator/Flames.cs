﻿using System.Collections.Generic;
using System.Linq;

namespace LoveCalculator
{
    public class Flames
    {
        private readonly Dictionary<char, string> _flamesMapping  = new Dictionary<char, string>
        {
            { 'F', "Friends" },
            { 'L', "Lovers" },
            { 'A', "Attraction" },
            { 'M', "Married" },
            { 'E', "Enemies" },
            { 'S', "Siblings" },
        };

        private readonly Dictionary<string, string> _flamesMeaning = new Dictionary<string, string>
        {
            { "Friends", "You both will have an amazing friendship forever" },
            { "Lovers", "You guys love each other. True lovers" },
            { "Attraction", "You are attracted to each other for many reasons" },
            { "Married", "You will get married to your partner very soon" },
            { "Enemies", "Lot of hate. You will be enemies and won't take to each other" },
            { "Siblings", "You both are great siblings and care for each other" },
        };

        public string GetResult(string yourName, string partnerName)
        {
            var commonLetters = new List<char>(string.Concat(yourName.Replace(" ", "").Intersect(partnerName.Replace(" ", ""))));

            var yourNameDelta = TrimByLetters(yourName, commonLetters);
            var partnerNameDelta = TrimByLetters(partnerName, commonLetters);
            var remainingLetterCount = yourNameDelta.Count() + partnerNameDelta.Count();
            var flamesTemp = "FLAMES";

            while (flamesTemp.Count() > 1)
            {
                int remainder = remainingLetterCount % flamesTemp.Length;
                var index = remainder != 0 ? remainder -1 : flamesTemp.Length - 1;
                flamesTemp = flamesTemp.Remove(index, 1);
                flamesTemp = Arrange(flamesTemp, index);
            }
            string flamesResult;
            _flamesMapping.TryGetValue(flamesTemp.ToCharArray()[0], out flamesResult);
            return flamesResult;
        }

        public string GetMeaning(string flamesResult)
        {
            string meaning;
            _flamesMeaning.TryGetValue(flamesResult, out meaning);
            return meaning;
        }

        private string TrimByLetters(string text, List<char> letters)
        {
            foreach (var letter in letters)
            {
                text = text.Replace(letter.ToString(), "");
            }
            return text;
        }

        private string Arrange(string originalText, int pivot)
        {
            string firstHalf = originalText.Substring(pivot);
            string secondHalf = string.Concat(originalText.Take(pivot));
            return firstHalf + secondHalf;
        }
    }
}
