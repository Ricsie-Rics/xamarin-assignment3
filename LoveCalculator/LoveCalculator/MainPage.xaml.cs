﻿using System;
using Xamarin.Forms;

namespace LoveCalculator
{
	public partial class MainPage : ContentPage
	{
        public string developerName { get; } = "Rics";

        public MainPage()
		{
			InitializeComponent();
            imgHeart.Source = ImageSource.FromResource("LoveCalculator.Resources.heart.png"); //Build Action: Embedded resource
        }

        private void OnCheckFlamesClick(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(entYourName.Text))
            {
                DisplayAlert("Error", "Your name is empty", "Close");
                return;
            }
            if (string.IsNullOrWhiteSpace(entPartnerName.Text))
            {
                DisplayAlert("Error", "Partner name is empty", "Close");
                return;
            }
            var flames = new Flames();
            string flamesResult = flames.GetResult(entYourName.Text, entPartnerName.Text);
            resultBox.RelationshipResultText = flamesResult;
            resultBox.MeaningResultText = flames.GetMeaning(flamesResult);
            resultBox.IsVisible = true;
        }
    }
}
