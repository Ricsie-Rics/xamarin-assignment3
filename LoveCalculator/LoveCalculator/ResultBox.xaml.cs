﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace LoveCalculator
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ResultBox : ContentView
	{
		public ResultBox ()
		{
			InitializeComponent ();
		}

        public string RelationshipResultText
        {
            get { return relationshipResult.Text; }
            set { relationshipResult.Text = value; }
        }

        public string MeaningResultText
        {
            get { return meaningResult.Text; }
            set { meaningResult.Text = value; }
        }
    }
}